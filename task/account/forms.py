from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model,authenticate

from django.forms.utils import ValidationError
from . import models
class RegistrationForm(forms.ModelForm):
    name = forms.CharField(max_length=100, required=True)

    class Meta:
      model=models.Models
      fields=['name']


User=get_user_model()
class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                raise forms.ValidationError('Invalid credentials!')
            if not user.check_password(password):
                raise forms.ValidationError('Incorrect password')
            if not user.is_active:
                raise forms.ValidationError('This user is not active')
        return super(UserLoginForm, self).clean(*args, **kwargs)

		  
