from django.shortcuts import render,redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.http import HttpResponse
from .forms import RegistrationForm,UserLoginForm
from .models import Models
from django.contrib.auth import authenticate, login, logout

@login_required(login_url='/account/login/')
@transaction.atomic
def registerModel(request):
   
    if request.method=='POST':
        form = RegistrationForm(request.POST)
         #instantiate the form

        if form.is_valid():
            mform = form.save() #save all the details
            mform.save() #save a model instance to the db
            return redirect('/account/create/')


        else:
            #if any of the details in the form is invalid we render the form again
            print(form.errors)
            return render(request,'account/login.html', {'form':form})
    else:
        form = RegistrationForm()

    return render(request,'account/login.html', {'form':form})

       
   


@login_required(login_url='/account/login/')
@transaction.atomic
def delRecord(request,m_id):
    x=Models.objects.filter(id=m_id)
    if x:
        x.delete()
        return HttpResponse("Successfull")
    else:
        return HttpResponse("Not Found")
    
    


@login_required(login_url='/account/login/')
@transaction.atomic
def findrecord(request):
    if request.method=='POST':
        form=RegistrationForm(request.POST)
        #print(form.name)
        nam=form.data['name']
   
        mod = Models.objects.filter(name=nam)
        if mod :
            return HttpResponse(mod)
        else:
            return HttpResponse('Not Found')
    else:
        form=RegistrationForm()
    return render(request,'account/login.html', {'form':form})    
  
    #return render(request,'account/login.html', {'form':form})


@login_required(login_url='/account/login/')
@transaction.atomic
def listrecords(request):
    all_records = Models.objects.all()
    return  HttpResponse(all_records)




@login_required(login_url='/account/login/')
@transaction.atomic
def edit(request, row_id):
    model = get_object_or_404(Models, pk=row_id)
    form = RegistrationForm(request.POST or None, instance=model)

    if request.method == "POST" and form.is_valid():
        form.save()
        return HttpResponse("Successful")

    context = {'form': model}
    return render(request, 'account/edit.html', context)



def user_login(request):
    next = request.GET.get('next')
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        
        user = authenticate(username=username, password=password)

        login(request, user)
        
        if user.is_authenticated:
            return HttpResponse("Login Successfull")
        
        return redirect('/account/login/')

    context = {
        'form': form,
    }
    print(form.errors)
    return render(request, "account/login.html", context)


def user_logout(request):
    logout(request)
    return HttpResponse("Logged out")
