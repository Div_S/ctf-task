from .import views
from django.urls import include, path


urlpatterns = [
path('create/', views.registerModel, name='create'),
path('edit/<int:row_id>/', views.edit, name='edit'),
path('delete/<int:m_id>/', views.delRecord, name='del'),
path('list/', views.listrecords, name='list'),
path('find/', views.findrecord, name='find'),
path('login/',views.user_login,name='login'),
path('logout/',views.user_logout,name='logout'),

]